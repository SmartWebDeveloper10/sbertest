package com.example.SberTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

@SpringBootApplication
public class SberTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SberTestApplication.class, args);
	}

}
