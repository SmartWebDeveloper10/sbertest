package com.example.SberTest.entities;


import com.example.SberTest.entities.validators.QuoteConstraint;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "quote")
@QuoteConstraint
public class Quote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "isin")
    @JsonProperty(value = "ISIN")
    private String isin;

    @Column(name = "bid")
    @JsonProperty(value = "Bid")
    private double bid=-1;

    @Column(name = "ask")
    @JsonProperty(value = "Ask")
    private double ask;


}
