package com.example.SberTest.entities.validators;

import com.example.SberTest.entities.Quote;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class QuoteValidator implements
        ConstraintValidator<QuoteConstraint, Quote> {

    @Override
    public void initialize(QuoteConstraint constraint) {
    }

    @Override
    public boolean isValid(Quote value, ConstraintValidatorContext context) {

        boolean res = value.getAsk()>value.getBid() && value.getIsin().length()==12;
        return res;
    }
}
