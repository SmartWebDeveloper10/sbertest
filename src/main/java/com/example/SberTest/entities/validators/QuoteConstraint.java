package com.example.SberTest.entities.validators;

import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {QuoteValidator.class})
public @interface QuoteConstraint {
    String message() default "Invalid isin or ask<bid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
