package com.example.SberTest.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "elvl")
public class Elvl {


    @Id
    @Length(max = 12)
    @Column(name = "ISIN")
    private String isin;

    @Column(name="ELVL")
    private double elvl;
}
