package com.example.SberTest.services;

import com.example.SberTest.controllers.QuoteController;
import com.example.SberTest.entities.Elvl;
import com.example.SberTest.entities.Quote;

import java.util.List;
import java.util.Optional;

public interface ElvlService {

    Optional<Elvl> getElvlByIsin(String isin);

    List<Elvl> getAllElvl();

    Elvl updateElvlFromQuote(Elvl e, Quote q);

    Elvl save(Elvl e);
}
