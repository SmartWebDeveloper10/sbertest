package com.example.SberTest.services;


import com.example.SberTest.entities.Quote;

public interface QuoteService {

    Quote save(Quote q);
}
