package com.example.SberTest.services.impl;

import com.example.SberTest.entities.Elvl;
import com.example.SberTest.entities.Quote;
import com.example.SberTest.repositories.ElvlRepository;
import com.example.SberTest.repositories.QuoteRepository;
import com.example.SberTest.services.ElvlService;
import com.example.SberTest.services.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class QuoteServiceImpl implements QuoteService {
    @Autowired
    QuoteRepository quoteRepository;

    @Autowired
    ElvlRepository elvlRepository;

    @Autowired
    ElvlService elvlService;

    @Override
    @Transactional
    public Quote save(Quote q){
        Optional<Elvl> elvl = elvlRepository.findByIsin(q.getIsin());
        Elvl updatedElvl;

        if(elvl.isPresent()){
            updatedElvl = elvlService.updateElvlFromQuote(elvl.get(),q);
        }
        else{
            updatedElvl = elvlService.updateElvlFromQuote(null,q);
        }

        elvlRepository.saveAndFlush(updatedElvl);
        return quoteRepository.saveAndFlush(q);
    }
}
