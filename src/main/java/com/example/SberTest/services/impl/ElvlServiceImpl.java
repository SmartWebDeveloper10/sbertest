package com.example.SberTest.services.impl;

import com.example.SberTest.entities.Elvl;
import com.example.SberTest.entities.Quote;
import com.example.SberTest.repositories.ElvlRepository;
import com.example.SberTest.services.ElvlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ElvlServiceImpl implements ElvlService {

    @Autowired
    private ElvlRepository elvlRepository;


    @Override
    public Optional<Elvl> getElvlByIsin(String isin) {
        return elvlRepository.findById(isin);
    }

    @Override
    public List<Elvl> getAllElvl() {
        return elvlRepository.findAll();
    }

    @Override
    public Elvl updateElvlFromQuote(Elvl e, Quote q) {
        if(e==null){
            return Elvl.builder().elvl(q.getBid()).isin(q.getIsin()).build();
        }
        else if(q.getBid()>=0 && q.getBid()>e.getElvl()){
            e.setElvl(q.getBid());
            return e;

        }
        else if(q.getBid()<0 || q.getBid()<e.getElvl()){
            e.setElvl(q.getAsk());
            return e;
        }
        else{
            return null;
        }
    }

    @Override
    public Elvl save(Elvl e) {
        return elvlRepository.save(e);
    }


}
