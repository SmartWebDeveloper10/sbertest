package com.example.SberTest.controllers;


import com.example.SberTest.entities.Elvl;
import com.example.SberTest.services.ElvlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/Elvls")
public class ElvlController {

    @Autowired
    ElvlService elvlService;

    @GetMapping("/getElvl/{isin}")
    public Double getElvl(@PathVariable("isin") String isin ){
        Optional<Elvl> result = elvlService.getElvlByIsin(isin);
        if(result.isPresent()){
            return result.get().getElvl();
        }
        else{
            return -1.0;
        }


    }

    @GetMapping("/getAllElvls")
    public List<Double> getAllElvls( ){

        return elvlService.getAllElvl().stream().map(v->v.getElvl()).collect(Collectors.toList());
    }
}
