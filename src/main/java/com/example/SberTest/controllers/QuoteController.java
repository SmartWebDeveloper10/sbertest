package com.example.SberTest.controllers;

import com.example.SberTest.entities.Quote;
import com.example.SberTest.services.QuoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/Quotes")
public class QuoteController {

    @Autowired
    private QuoteService quoteService;

      @RequestMapping(value = "/saveNewQuote",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Quote saveQuote( @Valid @RequestBody Quote quote ){

        return quoteService.save(quote);
    }
}
