package com.example.SberTest.repositories;

import com.example.SberTest.entities.Elvl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.Optional;


public interface ElvlRepository extends JpaRepository<Elvl,String> {

    //to process a few quotes that modify the same elvl
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT e FROM Elvl e WHERE e.isin =:isin")
    Optional<Elvl> findByIsin(@Param("isin") String isin);
}
