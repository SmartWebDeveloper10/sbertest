package com.example.SberTest.repositories;

import com.example.SberTest.entities.Quote;
import org.springframework.data.jpa.repository.JpaRepository;


public interface QuoteRepository extends JpaRepository<Quote, Long> {
}
