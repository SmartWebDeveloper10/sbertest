CREATE TABLE quote (
    id BIGINT NOT NULL AUTO_INCREMENT,
    isin VARCHAR(20),
    bid DECIMAL(20,5),
    ask DECIMAL(20,5) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE elvl (
    isin VARCHAR(20) NOT NULL UNIQUE,
    elvl DECIMAL(20,5),
    PRIMARY KEY (isin)
);