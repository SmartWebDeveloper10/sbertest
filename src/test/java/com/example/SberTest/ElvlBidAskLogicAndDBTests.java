package com.example.SberTest;

import com.example.SberTest.entities.Quote;
import com.example.SberTest.repositories.ElvlRepository;
import com.example.SberTest.repositories.QuoteRepository;
import com.example.SberTest.services.ElvlService;
import com.example.SberTest.services.QuoteService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = SberTestApplication.class)
class ElvlBidAskLogicAndDBTests {

	@Autowired
	ElvlRepository elvlRepository;

	@Autowired
	QuoteRepository quoteRepository;

	@Autowired
	ElvlService elvlService;

	@Autowired
	QuoteService quoteService;

	@BeforeEach
	void setup(){

		elvlRepository.deleteAll();
		elvlRepository.flush();

		quoteRepository.deleteAll();
		quoteRepository.flush();


	}

	@AfterEach
	public void closeUp(){

		elvlRepository.deleteAll();
		elvlRepository.flush();

		quoteRepository.deleteAll();
		quoteRepository.flush();

	}

	@Test
	void ifQuoteArrivesElvlIsCreatedAndSaved() {
		String isin = "RU000A0JX0J2";
		double bid = 100.2;
		double ask = 101.9;

		Quote q = Quote.builder().isin(isin).ask(ask).bid(bid).build();

		quoteService.save(q);

		assertEquals(quoteRepository.findAll().size(),1);
		assertEquals(quoteRepository.findAll().get(0).getIsin(),isin);
		assertEquals(elvlRepository.findById(isin).get().getElvl(),bid);

	}

	@Test
	void ifAnotherQuoteWithTheSameIsinArrivesElvlIsUpdated() {
		String isin = "RU000A0JX0J2";
		double bid = 100.2;
		double ask = 101.9;

		double bid1 = 101.2;
		double ask1 = 103.9;

		Quote q = Quote.builder().isin(isin).ask(ask).bid(bid).build();

		quoteService.save(q);

		Quote q1 = Quote.builder().isin(isin).ask(ask1).bid(bid1).build();

		quoteService.save(q1);

		assertEquals(quoteRepository.findAll().size(),2);
		assertEquals(elvlRepository.findAll().size(),1);
		assertEquals(elvlRepository.findById(isin).get().getElvl(),bid1);

	}

	@Test
	void ifTwoQuotedWithDifferentIsinArriveTwoElvlsAreSaved() {
		String isin = "RU000A0JX0J2";
		double bid = 100.2;
		double ask = 101.9;

		String isin1 = "RU000A0JX0J3";
		double bid1 = 101.2;
		double ask1 = 103.9;

		Quote q = Quote.builder().isin(isin).ask(ask).bid(bid).build();

		quoteService.save(q);

		Quote q1 = Quote.builder().isin(isin1).ask(ask1).bid(bid1).build();

		quoteService.save(q1);

		assertEquals(quoteRepository.findAll().size(),2);
		assertEquals(elvlRepository.findAll().size(),2);
		assertEquals(elvlRepository.findById(isin1).get().getElvl(),bid1);
		assertEquals(elvlRepository.findById(isin).get().getElvl(),bid);

	}

	@Test
	void ifBidGTElvlThenEvlEqBid() {
		String isin = "RU000A0JX0J2";
		double bid = 100.2;
		double ask = 101.9;

		double bid1 = 101.2;
		double ask1 = 103.9;

		Quote q = Quote.builder().isin(isin).ask(ask).bid(bid).build();

		quoteService.save(q);

		Quote q1 = Quote.builder().isin(isin).ask(ask1).bid(bid1).build();

		quoteService.save(q1);

		assertEquals(quoteRepository.findAll().size(),2);
		assertEquals(elvlRepository.findAll().size(),1);
		assertEquals(elvlRepository.findById(isin).get().getElvl(),bid1);

	}

	@Test
	void ifAskLTElvlThenEvlEqAsk() {
		String isin = "RU000A0JX0J2";
		double bid = 105.2;
		double ask = 106.9;

		double bid1 = 101.2;
		double ask1 = 103.9;

		Quote q = Quote.builder().isin(isin).ask(ask).bid(bid).build();

		quoteService.save(q);

		Quote q1 = Quote.builder().isin(isin).ask(ask1).bid(bid1).build();

		quoteService.save(q1);

		assertEquals(quoteRepository.findAll().size(),2);
		assertEquals(elvlRepository.findAll().size(),1);
		assertEquals(elvlRepository.findById(isin).get().getElvl(),ask1);

	}

}
