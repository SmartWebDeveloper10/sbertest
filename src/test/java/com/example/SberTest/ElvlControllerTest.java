package com.example.SberTest;


import com.example.SberTest.controllers.ElvlController;
import com.example.SberTest.controllers.QuoteController;
import com.example.SberTest.entities.Elvl;
import com.example.SberTest.entities.Quote;
import com.example.SberTest.services.ElvlService;
import com.example.SberTest.services.QuoteService;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(ElvlController.class)
@AutoConfigureMockMvc
public class ElvlControllerTest {

    @Autowired
    private MockMvc server;

    @MockBean
    private ElvlService elvlService;

    @Test
    void getElvlByIsin() throws Exception {
        String isin = "RU000A0JX0J2";
        double elvl = 105.2;



        Elvl el = Elvl.builder().elvl(elvl).isin(isin).build();

        String URL = "/Elvls/getElvl/"+isin;

        when(elvlService.getElvlByIsin(isin)).thenReturn(Optional.of(el));

        server
                .perform(get(URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString()
                .contains( String.valueOf(elvl) );

        verify(elvlService, times(1)).getElvlByIsin(isin);

    }

    @Test
    void getAllElvls() throws Exception {

        double elvl1 = 105.2;

        String URL = "/Elvls/getAllElvls";

        List<Elvl> list = new ArrayList<>();
        list.add(Elvl.builder().isin("ABC").elvl(elvl1).build());

        when(elvlService.getAllElvl()).thenReturn(list);

        server
                .perform(get(URL))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString()
                .contains( String.valueOf(elvl1) );

        verify(elvlService, times(1)).getAllElvl();

    }
}
