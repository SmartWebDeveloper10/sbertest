package com.example.SberTest;


import com.example.SberTest.controllers.QuoteController;
import com.example.SberTest.entities.Quote;
import com.example.SberTest.services.QuoteService;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;

import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.mockito.Mockito.when;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(QuoteController.class)
@AutoConfigureMockMvc
public class QuoteControllerTest {

    @Autowired
    private MockMvc server;

   @MockBean
   private QuoteService quoteService;

    @Test
    void successSaveNewUser() throws Exception {
        String isin = "RU000A0JX0J2";
        double bid = 105.2;
        double ask = 106.9;

        Quote q = Quote.builder().isin(isin).ask(ask).bid(bid).build();

        Quote qout = Quote.builder().isin(isin).ask(ask).bid(bid).id(0L).build();

        String URL = "/Quotes/saveNewQuote";

        when(quoteService.save(q)).thenReturn(qout);

        server
                .perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(q)))
                .andDo(print())
                .andExpect(status().isOk());

        verify(quoteService, times(1)).save(q);

    }

    @Test
    void failedSaveNewUserIfWrongIsin() throws Exception {
        String isin = "RU000A0JX0J";
        double bid = 105.2;
        double ask = 106.9;

        Quote q = Quote.builder().isin(isin).ask(ask).bid(bid).build();

        Quote qout = Quote.builder().isin(isin).ask(ask).bid(bid).id(0L).build();

        String URL = "/Quotes/saveNewQuote";

        when(quoteService.save(q)).thenReturn(qout);

        server
                .perform(post(URL)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(q)))
                .andDo(print())
                .andExpect(status().is(400));

        verify(quoteService, times(0)).save(q);

    }
}
